package ru.khamitov.contract.repository;

import org.springframework.data.repository.CrudRepository;
import ru.khamitov.contract.model.Contract;

public interface ContractRepository extends CrudRepository <Contract, Long>{
}

package ru.khamitov.contract.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khamitov.contract.model.Contract;
import ru.khamitov.contract.repository.ContractRepository;

@Service
public class ContactServiceImpl <E extends Contract> implements ContractService <E> {

    @Autowired
    ContractRepository contractRepository;

    @Override
    public E find(Long id) {
        return (E) contractRepository.findOne(id);
    }

    @Override
    public <S extends E> S save(S contract) {
        return contractRepository.save(contract);
    }

    @Override
    public Iterable<E> findAll() {
        return (Iterable<E>) contractRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        contractRepository.delete(id);
    }

    @Override
    public void delete(E contract) {
        contractRepository.delete(contract);
    }
}

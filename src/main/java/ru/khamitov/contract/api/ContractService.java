package ru.khamitov.contract.api;

import ru.khamitov.contract.model.Contract;

public interface ContractService <E extends Contract> {
    E find(Long id);
    <S extends E> S save(S contract);
    Iterable<E> findAll();
    void delete(Long id);
    void delete(E contract);
}

package ru.khamitov.contract.model;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class DeclarationOfSale extends Contract {

    private LocalDate dateOfFeed;

    public LocalDate getDateOfFeed() {
        return dateOfFeed;
    }

    public void setDateOfFeed(LocalDate dateOfFeed) {
        this.dateOfFeed = dateOfFeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeclarationOfSale)) return false;
        if (!super.equals(o)) return false;
        DeclarationOfSale that = (DeclarationOfSale) o;
        return Objects.equals(dateOfFeed, that.dateOfFeed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dateOfFeed);
    }

}

package ru.khamitov.contract.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Objects;

@Entity
public class ReportOfSale extends Contract {
    private SaleBuyContract saleBuyContract;

    @OneToOne
    @JoinColumn()
    public SaleBuyContract getSaleBuyContract() {
        return saleBuyContract;
    }

    public void setSaleBuyContract(SaleBuyContract saleBuyContract) {
        this.saleBuyContract = saleBuyContract;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReportOfSale)) return false;
        if (!super.equals(o)) return false;
        ReportOfSale that = (ReportOfSale) o;
        return Objects.equals(saleBuyContract, that.saleBuyContract);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), saleBuyContract);
    }
}

package ru.khamitov.contract.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.khamitov.contract.model.Contract;
import ru.khamitov.contract.model.DeclarationOfSale;
import ru.khamitov.contract.model.ReportOfSale;
import ru.khamitov.contract.model.SaleBuyContract;
import ru.khamitov.contract.repository.ContractRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractServiceTest {

    @Autowired
    ContractService<SaleBuyContract> saleBuyContractService;
    @Autowired
    ContractService<DeclarationOfSale> declarationOfSaleContractService;
    @Autowired
    ContractService<ReportOfSale> reportOfSaleContractService;
    @Autowired
    ContractService<Contract> contractService;
    @Autowired
    ContractRepository contractRepository;

    @Test
    public void testFindById() {
        SaleBuyContract saleBuyContract = produceTransientSaleBuyContract();
        Long id = contractRepository.save(saleBuyContract).getId();

        SaleBuyContract contract = saleBuyContractService.find(id);

        assertEqualsContractFields(saleBuyContract, contract);
        assertEquals(saleBuyContract.getAmount(), contract.getAmount());
    }

    @Test
    public void testSave() {
        DeclarationOfSale declarationOfSale = produceTransientDeclarationOfSale();

        Long id = declarationOfSaleContractService.save(declarationOfSale).getId();

        DeclarationOfSale contract = (DeclarationOfSale) contractRepository.findOne(id);
        assertEqualsContractFields(declarationOfSale, contract);
        assertEquals(declarationOfSale.getDateOfFeed(), contract.getDateOfFeed());
    }

    @Test
    public void testFindAll() {
        DeclarationOfSale declarationOfSale = produceTransientDeclarationOfSale();
        SaleBuyContract saleBuyContract = produceTransientSaleBuyContract();
        ReportOfSale reportOfSale = produceTransientReportOfSale(saleBuyContract);

        contractRepository.save(Arrays.asList(declarationOfSale, saleBuyContract, reportOfSale));

        List<Contract> contracts = (List<Contract>) contractService.findAll();
        assertTrue(contracts.size() == 3);

    }

    @Test
    public void testDeleteById() {
        ReportOfSale reportOfSale = produceTransientReportOfSale(contractRepository.save(produceTransientSaleBuyContract()));
        Long id = contractRepository.save(reportOfSale).getId();

        reportOfSaleContractService.delete(id);

        assertNull(contractRepository.findOne(id));
    }

    @Test
    public void testDeleteByInstance() {
        ReportOfSale reportOfSale = produceTransientReportOfSale(contractRepository.save(produceTransientSaleBuyContract()));
        ReportOfSale savedReport = contractRepository.save(reportOfSale);

        reportOfSaleContractService.delete(savedReport);

        assertNull(contractRepository.findOne(savedReport.getId()));
    }

    private DeclarationOfSale produceTransientDeclarationOfSale() {
        DeclarationOfSale declarationOfSale = (DeclarationOfSale) produceTransientContract(new DeclarationOfSale());
        declarationOfSale.setDateOfFeed(LocalDate.now());

        return declarationOfSale;
    }

    private SaleBuyContract produceTransientSaleBuyContract() {
        SaleBuyContract saleBuyContract = (SaleBuyContract) produceTransientContract(new SaleBuyContract());
        saleBuyContract.setAmount(BigDecimal.valueOf(10, 2));

        return saleBuyContract;
    }

    private ReportOfSale produceTransientReportOfSale(SaleBuyContract saleBuyContract) {
        ReportOfSale reportOfSale = (ReportOfSale) produceTransientContract(new ReportOfSale());
        reportOfSale.setSaleBuyContract(saleBuyContract);

        return reportOfSale;
    }

    private Contract produceTransientContract(Contract contract) {
        contract.setAuthor("Khamitov");
        contract.setCreationDate(LocalDate.now());
        contract.setNumber("63253561623173131-УВН");

        return contract;
    }

    private void assertEqualsContractFields(Contract expected, Contract actual) {
        assertEquals(expected.getAuthor(), actual.getAuthor());
        assertEquals(expected.getCreationDate(), actual.getCreationDate());
        assertEquals(expected.getNumber(), actual.getNumber());
    }
}
